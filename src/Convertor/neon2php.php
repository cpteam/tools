<?php

use CPTeam\Tools\Php\ConfigGenerator;
use Nette\Neon\Neon;

require __DIR__ . "/../../vendor/autoload.php";

$neon
	= "parameters:
	logDir: %appDir%../log

	poeditor:
		apiUrl: https://poeditor.com/api/
		apiKey: a7b765a3aeb41d46e1771f207ac04705
		projectId: 65401
		dirs:
			save: %appDir%/lang
			backup: %appDir%/lang/backup
	database:
		host: dmpm-mysql
		user: dmpm
		password: erTHeR
		name: dmpm

	# Resources
	sassPath: %appDir%/../resources/css/sass/styles.sass
	sassPathOutput: %appDir%/../resources/css/styles.css
	assetsDirName: assets
	assetsDir: %wwwDir%/%assetsDirName%

	# Domains
	domain: dmpm.docker
	cookieDomain: .%domain%

	# Int's
	MIN_SESSIONS_TO_TAG: 1000
	taxonCache: 1

	# Bools
	showAdformPixel: false
	logEvent: false
	deployedMode: false

	# DMPM - TV
	DMPM-TV:
		oauthAppId: null
		statsUrl: http://api.dmpm-tv.docker/stats/tv";

$array = Neon::decode($neon);

$gen = new ConfigGenerator($array);

echo $gen->generate();

